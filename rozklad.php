<?php require 'header.php';?>

<div class="container">
    <h1>Робочий навчальний план </h1>
</div> 
    <div class="table-wrap">
    <table class="table table-bordered ">
      <thead class="center">
       <table>
		     <tr>
			  <th><b>Предмети:</b></th>
			  <th>Основи WEB програмування</th>
       		  <th>Правознавство</th>
			  <th>Теорія автоматичного керування</th>
	   		  <th>Системи проектування, ідентифікації та моделювання</th>
	   		  <th>Електроніка та мікропроцесорна техніка</th>
	   		  <th>Числові методи і моделювання на ЕОМ</th>
			  <th>Автоматизація технологічних процесів</th>
	  		 </tr>
			 
              <td>Години:</td>
              <td>30</td>
              <td>20</td>
              <td>100</td>
              <td>100</td>
              <td>90</td>
              <td>70</td>
              <td>90</td>
	  		 </tr>
	  		 <tr>
              <td>Лекційні години:</td>
              <td>30</td>
              <td>20</td>
              <td>100</td>
              <td>100</td>
              <td>90</td>
              <td>70</td>
              <td>90</td>
	 		 </tr>
	  		 <tr>
              <td>Практичні години:</td>
              <td>30</td>
              <td>20</td>
              <td>100</td>
              <td>100</td>
              <td>90</td>
              <td>70</td>
              <td>90</td>
	  		 </tr>
	  		 <tr>
              <td>Лабораторні години:</td>
              <td>30</td>
              <td>20</td>
              <td>100</td>
              <td>100</td>
              <td>90</td>
              <td>70</td>
              <td>90</td>
             </tr>
			 <tr>
              <td>Кредити:</td>
              <td>30</td>
              <td>20</td>
              <td>100</td>
              <td>100</td>
              <td>90</td>
              <td>70</td>
              <td>90</td>
             </tr>
	 		</table>
			
		</div> 	
	 <hr/>

<div class="container">
       <footer>&copy; Всі права захищені. 2019 @marydemch. </footer>
 <div>    
</body>
</html>